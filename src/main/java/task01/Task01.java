package task01;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 */
public class Task01 implements GreatestCommonDivisor {


    @Override
    public int standard(int a, int b) {
        List<Integer> listA = new ArrayList<>();
        List<Integer> listB = new ArrayList<>();

        while (a != 1) {
            for (int i = 2; i <= a; i++) {
                if (a % i == 0) {
                    a = a / i;
                    listA.add(i);
                    break;
                }

            }
        }

        while (b != 1) {
            for (int i = 2; i <= b; i++) {
                if (b % i == 0) {
                    b = b / i;
                    listB.add(i);
                    break;
                }

            }
        }

        listA.retainAll(listB);

        Set<Integer> setA = new HashSet<>(listA);
        int resultA = 1;

        for (int num : setA) {
            resultA *= num;
        }

        return resultA;
    }

    @Override
    public int euclidean(int a, int b) throws NegativeNumberException {
        if ((a < 0) || (b < 0)) {
            throw new NegativeNumberException("Number can not be negative!");
        } else {

            if (b == 0) return a;


            while (a != b) {
                if (a > b) {
                    a -= b;
                } else {
                    b -= a;
                }
            }
        }

        return a;
    }

    @Override
    public int binary(int a, int b) {
        int d = 1;
        while (a != b) {
            if ((a % 2 == 0) && (b % 2 == 0)) {
                a = a / 2;
                b = b / 2;
                d *= 2;
            } else if (a % 2 == 0) {
                a = a / 2;
            } else if (b % 2 == 0) {
                b = b / 2;
            } else if (a > b) {
                a = (a - b) / 2;
            } else {
                b = (b - a) / 2;
            }
        }


        return (a * d);
    }

    // Least common multiple
    public int leastCommonMultiple(int a, int b) throws NegativeNumberException {

        return ((a * b) / euclidean(a, b));

    }
}
