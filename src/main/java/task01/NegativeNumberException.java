package task01;

/**
 *
 */
public class NegativeNumberException extends Exception {

    public NegativeNumberException(String message){
        super(message);
    }
}
