package task01;

/**
 *
 */
public interface GreatestCommonDivisor {

    int standard(int a, int b);
    int euclidean(int a, int b) throws NegativeNumberException;
    int binary(int a, int b);
}
