package task03;

/**
 *
 */
public class Task03 {

    public int[] kArrayRotation(int[] A, int k) {

        if (A.length == 0) return new int[0];
        if (A.length == 1) return A;
        if (k == 0) return A;
        if (k < 0) return new int[0];

        int arrayLength = A.length;
        int[] B = new int[arrayLength];

        for (int i = 0; i < arrayLength; i++) {
            if (i < k) {
                B[i] = A[arrayLength - k + i];
            } else {
                B[i] = A[i - k];
            }

        }

        return B;
    }

    public int[] altKRotation(int[] A, int k) {

        if (A.length == 0) return new int[0];
        if (A.length == 1) return A;
        if (k == 0) return A;
        if (k < 0) return new int[0];

        int arrayLength = A.length;

        for (int i = 0; i < k; i++) {
            for (int j = arrayLength - 1; j > 0; j--) {

                int a = A[j];
                A[j] = A[j - 1];
                A[j - 1] = a;

            }
        }

        return A;
    }

}
