package task02;

/**
 *
 */
public class Task02 {

    public Point projectPointOntoLine(Point P1, Point P2, Point P) {
        double dX = (P2.getX()-P1.getX());
        double dY = (P2.getY()-P1.getY());

        double diffX = (P1.getX()-P2.getX());
        double diffY = (P1.getY()-P2.getY());

        double u = (((P.getX() - P1.getX()) * dX) + ((P.getY() - P2.getY()) * dY))/((diffX*diffX)
                +(diffY*diffY));

        double x = dX*u+P1.getX();
        double y = dY*u+P1.getY();

        return new Point(x,y);
    }
}