package task04;

import java.util.Arrays;

/**
 *
 */
public class Task04 {

    public int solution(int[] A) {

        int L = A.length;
        int occurrence = L / 2;

        Arrays.sort(A);

        if (L == 0) return -1;
        if (L == 1) return A[0]; // it is correct if L/2 = 0;

        if (L == 2) {
            return A[0] == A[1] ? A[0] : -1;
        }

        int counter = 0;
        for (int i = 0; i < L; i++) {
            for (int j = i + 1; j <= occurrence + 1; j++) {
                if (A[i] == A[j]) {
                    counter++;
                } else {
                    break;
                }
            }

            if (counter > occurrence) return A[i];
        }


        return -1;
    }

    public int oneLoopSolution(int[] A) {
        int tableLength = A.length;
        int occurrence = tableLength / 2;
        int counter = 1;
        int element = A[0];

        Arrays.sort(A);

        if (tableLength == 0) return -1;
        if (tableLength == 1) return A[0];

        if (tableLength == 2) {
            return A[0] == A[1] ? A[0] : -1;
        }

        for (int i = 1; i < tableLength; i++) {
            if (A[i] == element) {
                counter++;
                if (counter > occurrence) return A[i - 1];
                } else {
                    element = A[i];
                    counter = 1;
                }
            }

        return -1;
    }

}


