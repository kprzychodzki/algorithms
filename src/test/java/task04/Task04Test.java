package task04;


import static org.assertj.core.api.Assertions.*;

import org.junit.Before;
import org.junit.Test;


/**
 *
 */

public class Task04Test {

    private Task04 task04 = new Task04();

    @Before
    public void setUp() {
        this.task04 = new Task04();
    }

    @Test
    public void test1() {
        int[] A = new int[]{};
        assertThat(task04.solution(A)).isEqualTo(-1);

    }

    @Test
    public void test2() {
        int[] A = new int[]{1};
        assertThat(task04.solution(A)).isEqualTo(1);
    }

    @Test
    public void test3() {
        int[] A = new int[]{1, 2};
        assertThat(task04.solution(A)).isEqualTo(-1);
    }

    @Test
    public void test4() {
        int[] A = new int[]{1, 1};
        assertThat(task04.solution(A)).isEqualTo(1);
    }

    @Test
    public void test5() {
        int[] A = new int[]{9, 9, 9, 1};
        assertThat(task04.solution(A)).isEqualTo(9);
        assertThat(task04.oneLoopSolution(A)).isEqualTo(9);
    }

    @Test
    public void test6() {
        int[] A = new int[]{2, 2, 2, 7, 1, 5};
        assertThat(task04.solution(A)).isEqualTo(-1);
        assertThat(task04.oneLoopSolution(A)).isEqualTo(-1);
    }

    @Test
    public void test7() {
        int[] A = new int[]{2, 2, 2, 2, 1, 5};
        assertThat(task04.solution(A)).isEqualTo(2);
        assertThat(task04.oneLoopSolution(A)).isEqualTo(2);
    }


}
