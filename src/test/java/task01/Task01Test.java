package task01;

import static org.assertj.core.api.Assertions.*;

import com.google.common.base.Stopwatch;
import org.junit.Before;
import org.junit.Test;

import java.util.concurrent.TimeUnit;

/**
 *
 */

public class Task01Test {

    private Task01 task01;
    final Stopwatch stopwatch = Stopwatch.createUnstarted();

    @Before
    public void setUp() {
        this.task01 = new Task01();
    }

    @Test
    public void testStandardGCD() {

        stopwatch.start();
        assertThat(task01.standard(525, 2310)).isEqualTo(105);
        stopwatch.stop();
        System.out.println(stopwatch.elapsed(TimeUnit.MILLISECONDS));
    }

    @Test
    public void testEuclideanGCD() {
        stopwatch.reset();
        stopwatch.start();
        try {
            assertThat(task01.euclidean(525,2310)).isEqualTo(105);
        } catch (NegativeNumberException e) {
            e.printStackTrace();
        }
        stopwatch.stop();
        System.out.println(stopwatch.elapsed(TimeUnit.MICROSECONDS));
    }

    @Test
    public void testBinaryGCD() {
        stopwatch.reset();
        stopwatch.start();
        assertThat(task01.binary(525,2310)).isEqualTo(105);
        stopwatch.stop();
        System.out.println(stopwatch.elapsed(TimeUnit.MICROSECONDS));
    }

    @Test
    public void testLCM() {
        stopwatch.reset();
        stopwatch.start();
        try {
            assertThat(task01.leastCommonMultiple(42,56)).isEqualTo(168);
        } catch (NegativeNumberException e) {
            e.printStackTrace();
        }
        stopwatch.stop();
        System.out.println(stopwatch.elapsed(TimeUnit.MICROSECONDS));
    }


}
