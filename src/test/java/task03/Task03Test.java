package task03;

import static org.assertj.core.api.Assertions.*;

import org.junit.Before;
import org.junit.Test;

/**
 *
 */
public class Task03Test {

    private Task03 task03 = new Task03();

    @Before
    public void setUp() {
        this.task03 = new Task03();
    }

    @Test
    public void test1() {
        int[] A = new int[]{3, 9, 5, 5, 1, 0, 5, 7};
        int k = 3;
        assertThat(task03.kArrayRotation(A, k)).isEqualTo(new int[]{0, 5, 7, 3, 9, 5, 5, 1});

    }

    @Test
    public void test2() {
        int[] A = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9};
        int k = 8;
        assertThat(task03.kArrayRotation(A, k)).isEqualTo(new int[]{2, 3, 4, 5, 6, 7, 8, 9, 1});
    }

    @Test
    public void shouldReturnEmptyArray() {
        int[] A = new int[0];
        int k = 2;
        assertThat(task03.kArrayRotation(A, k)).isEqualTo(new int[0]);
    }

    @Test
    public void shouldReturnA() {
        int[] A = new int[]{1};
        int k = 2;
        assertThat(task03.kArrayRotation(A, k)).isEqualTo(new int[]{1});
    }

    @Test
    public void shouldReturnUnrotatedA() {
        int[] A = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9};
        int k = 0;
        assertThat(task03.kArrayRotation(A, k)).isEqualTo(new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9});
    }

    @Test
    public void shouldReturnSameArrayWhenRotateSizeArrayTimes() {
        int[] A = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9};
        int k = 9;

        assertThat(task03.kArrayRotation(A, k)).isEqualTo(new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9});
    }

    @Test
    public void altKRotationTest() {
        int[] A = new int[]{3, 9, 5, 5, 1, 0, 5, 7};
        int k = 3;
        assertThat(task03.kArrayRotation(A, k)).isEqualTo(new int[]{0, 5, 7, 3, 9, 5, 5, 1});
    }
}
